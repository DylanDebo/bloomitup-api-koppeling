<?php

use Bloomitup\Controllers\BaseViewController;
use Bloomitup\Services\OrderDataService;

require_once dirname(__FILE__) . '/bootstrap.php';


if ($_POST && $_POST['orderBouquet'])
{
    print("<pre>" . print_r(OrderDataService::createOrder($_POST), true) . "</pre>");
    file_put_contents('./orderlog.log', 'ORDER PLACED' . PHP_EOL, FILE_APPEND);
} else {
    $view = new BaseViewController('orderBouquet');
    echo $view->render();
}