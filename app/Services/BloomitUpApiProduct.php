<?php


namespace Bloomitup\Services;


use GuzzleHttp\Client;

class BloomitUpApiProduct
{
    public static function getBloomitUpApiProduct () {
        $client = new Client();
        $data = json_decode($client->get($_ENV['BLOOMITUP_API_PRODUCT_ENDPOINT'])->getBody()->getContents());
        return (int) $data->product;
    }
}