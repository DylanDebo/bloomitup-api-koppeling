<?php

namespace Bloomitup\Services;

use Bloomitup\Api;
use Bloomitup\Repositories\CustomerRepository;
use Bloomitup\Repositories\OrderRepository;

class OrderDataService
{
    private static $customerData = [];

    public static function createOrder($data)
    {
        $orderData = self::prepareDataForOrder($data);
        return OrderRepository::createOrder($orderData);
    }

    private static function extractCustomerData()
    {
        $wcCustomerData = json_decode(json_encode(CustomerRepository::getCustomer()), true);
        self::$customerData = [
            'billing' => $wcCustomerData['billing']
        ];
    }

    private static function prepareDataForOrder($postData)
    {
        //Watch me cook some spaghetti
        $lineItems = [];
        array_push($lineItems, [
           'product_id' => BloomitUpApiProduct::getBloomitUpApiProduct(),
           'quantity' => 1
        ]);
        if (isset($postData['withCard']) && $postData['cardText']) {
            $cardData = [
                'product_id' => 8323,
                'quantity' => 1,
                'meta_data' => [
                    [
                        'key' => 'Tekst',
                        'value' => $postData['cardText']
                    ]
                ]
            ];
            array_push($lineItems, $cardData);
        }
        $data['shipping'] = $_POST['shipping'];
        $data['line_items'] = $lineItems;

        $standardData = [
            'customer_id' => $_ENV['WOOCOMMERCE_CUSTOMER_ID'],
            'payment_method' => 'cod',
            'payment_method_title' => 'Betaal op rekening'
        ];

        $data['shipping']['address_1'] = $data['shipping']['address_1'] . ', ' . $data['shipping']['house_number'];
        self::extractCustomerData();
        $orderData = array_merge(self::$customerData, $data, $standardData);
        try {
            if (!count($orderData['line_items']) > 0) {
                throw new \Exception('Geen producten aan order toegekend.');
            }
        } catch (\Exception $e) {
            die($e->getMessage());
        }
        return $orderData;
    }
}