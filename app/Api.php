<?php

namespace Bloomitup;

use Automattic\WooCommerce\Client;

class Api
{
    private static $instance;

    /**
     * @var Client
     */
    protected static $client;

    public static function setClient()
    {
        self::$client = new Client(
            $_ENV['WOOCOMMERCE_STORE_URL'],
            $_ENV['WOOCOMMERCE_CONSUMER_KEY'],
            $_ENV['WOOCOMMERCE_CONSUMER_SECRET'],
            [
                'version' => 'wc/v3'
            ]
        );
    }
}

Api::setClient();
