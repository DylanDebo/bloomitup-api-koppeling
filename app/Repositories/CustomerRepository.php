<?php

namespace Bloomitup\Repositories;

use Bloomitup\Api;

class CustomerRepository extends Api
{
    public static function getCustomer()
    {
        return self::$client->get("customers/${_ENV['WOOCOMMERCE_CUSTOMER_ID']}");
    }
}