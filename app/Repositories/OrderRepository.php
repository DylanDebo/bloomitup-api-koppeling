<?php


namespace Bloomitup\Repositories;


use Bloomitup\Api;

class OrderRepository extends Api
{
    public static function getOrder($id)
    {
        return self::$client->get("orders/${id}");
    }

    public static function createOrder($data)
    {
        return self::$client->post('orders', $data);
    }

    public static function updateOrder($id, $data)
    {
        return self::$client->put("orders/${id}", $data);
    }
}