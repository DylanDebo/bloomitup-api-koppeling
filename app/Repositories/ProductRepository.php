<?php

namespace Bloomitup\Repositories;

use Bloomitup\Api;

class ProductRepository extends Api
{
    public static function getProduct($id)
    {
        return self::$client->get("products/${id}");
    }
}