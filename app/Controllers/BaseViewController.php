<?php


namespace Bloomitup\Controllers;


class BaseViewController
{
    private $data = array();

    private $render = FALSE;

    private $file = '';

    public function __construct($template)
    {
        try {
            $this->file = $_ENV['BASE_APP_DIR'] . 'resources/views/' . $template . '.php';
            if (file_exists($this->file)) {
                $this->render = $this->file;
            } else {
                throw new \Exception('Template ' . $template . ' not found!');
            }
        }
        catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    public function s () {
        return 's';
    }

    public function render() {
        extract($this->data);

        ob_start();
        include($this->file);
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }

    public function assign($variable, $value)
    {
        $this->data[$variable] = $value;
    }

    public function __destruct()
    {
        extract($this->data);
    }
}