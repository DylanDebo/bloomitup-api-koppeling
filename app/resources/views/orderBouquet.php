
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?= $_ENV['BASE_ASSET_DIR']; ?>public/css/bootstrap/bootstrap.min.css"/>
    <link rel="icon" href="<?= $_ENV['BASE_ASSET_DIR']; ?>public/favicon.ico"/>
    <title>BloomitUp API</title>
</head>

<body>
<div class="d-flex flex-row w-100 justify-content-center p-5">
    <h2>BloomitUp bestel API</h2>
</div>
<div class="container d-flex justify-content-center">
    <div class="d-flex flex-column w-100">
        <p class="lead align-self-center">Vul hier de adresgegevens van het boeketje in!</p>
        <form method="post" class="d-flex flex-wrap">
                <div class="d-flex flex-column w-100 p-3">
                    <div class="form-group">
                        <label for="inputFirstName">Voornaam</label>
                        <input name="shipping[first_name]" type="text" class="form-control" id="inputFirstName" placeholder="Voornaam" required>
                    </div>
                    <div class="form-group">
                        <label for="inputLastName">Achternaam</label>
                        <input name="shipping[last_name]" type="text" class="form-control" id="inputLastName" placeholder="Achternaam" required>
                    </div>

                    <div class="form-group">
                        <label for="inputAddress">Adres</label>
                        <input name="shipping[address_1]" type="text" class="form-control" id="inputAddress" placeholder="Adres" required>
                    </div>
                    <div class="form-group">
                        <label for="inputHouseNumber">Huisnummer</label>
                        <input name="shipping[house_number]" type="text" class="form-control" id="inputHouseNumber" placeholder="Huisnummer" required>
                    </div>
                    <div class="form-group">
                        <label for="inputCity">Stad</label>
                        <input name="shipping[city]" type="text" class="form-control" id="inputAddress" placeholder="Stad" required>
                    </div>
                    <div class="form-group">
                        <label for="inputZipCode">Postcode</label>
                        <input name="shipping[postcode]" type="text" class="form-control" id="inputZipCode" placeholder="Postcode" required>
                    </div>
                    <div class="form-group">
                        <label for="inputCountry">Land</label>
                        <select name="shipping[country]" class="form-control" id="inputCountry" required>
                            <option selected value="NL">Nederland</option>
                            <option value="BE">België</option>
                        </select>
                    </div>
                    <div class="field-enabler">
                        <div class="form-group form-check">
                            <input name="withCard" type="checkbox" class="form-check-input toggles" id="inputWithCard" checked>
                            <label class="form-check-label" for="inputWithCard">Met kaartje?</label>
                        </div>
                        <div class="form-group toggleable">
                            <label for="inputCardText">Berichtje</label>
                            <textarea name="cardText" class="form-control toggleable-input" id="inputCardText" rows="3"></textarea>
                        </div>
                    </div>
                    <button type="submit" name="orderBouquet" value="yes" class="btn btn-primary">Verstuur!</button>
                </div>
        </form>
    </div>
</div><!-- /.container -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="<?= $_ENV['BASE_ASSET_DIR']; ?>public/js/bootstrap/bootstrap.min.js"></script>
<script src="<?= $_ENV['BASE_ASSET_DIR']; ?>public/js/fieldHelpers.js"></script>
</body>
</html>
