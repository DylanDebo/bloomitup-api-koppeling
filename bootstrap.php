<?php
require_once dirname(__FILE__) . '/vendor/autoload.php';

$dotEnv = \Dotenv\Dotenv::createImmutable(__DIR__);
$dotEnv->load();
$_ENV['BASE_APP_DIR'] = dirname(__DIR__) . '/Bloomitup-api-koppeling/app/';