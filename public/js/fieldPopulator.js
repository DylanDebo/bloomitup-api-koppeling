window.onload = function () {
    const queryString = window.location.search.substr(1).split("&")
    queryString.forEach(qs => {
        let keyValue = qs.split('=')
        let el = document.querySelector(`input[name$="shipping[${keyValue[0]}]"]`)
        if (el) {
            el.setAttribute('value', keyValue[1])
        }
    })
}