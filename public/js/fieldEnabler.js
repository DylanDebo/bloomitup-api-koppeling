window.onload = function () {
   let fieldEnablers = document.querySelectorAll('.field-enabler')
    fieldEnablers.forEach(fieldEnabler => {
        let toggleable = fieldEnabler.querySelector('.toggleable')
        fieldEnabler.querySelector('.toggles').addEventListener('click', function(event) {
            toggleable.classList.toggle('d-none')
        })
    })
}