window.onload = function () {
    const queryString = window.location.search.substr(1).split("&")
    queryString.forEach(qs => {
        let keyValue = qs.split('=')
        let el = document.querySelector(`input[name$="shipping[${keyValue[0]}]"]`)
        if (el) {
            el.setAttribute('value', keyValue[1])
        }
    })
    let fieldEnablers = document.querySelectorAll('.field-enabler')
    fieldEnablers.forEach(fieldEnabler => {
        let toggleable = fieldEnabler.querySelector('.toggleable')
        fieldEnabler.querySelector('.toggles').addEventListener('click', function(event) {
            toggleable.classList.toggle('d-none')
        })
    })
}